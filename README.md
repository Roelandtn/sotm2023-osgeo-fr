# OSGeo-fr presentation at SOTM France 2023

## Description

Work in progress

## Visuals

- [Slideshow](http://roelandtn.frama.io/sotm2023-osgeo-fr)

The presentation will be recorded. The link will be provided.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

There is an issue button somwhere in the project.

## Roadmap
Get ready for SOTM France 2023, give the presentation and forget about it.

## Contributing
Errors and fixes are welcome.

## Authors and acknowledgment
- Nicolas Roelandt 

## License

This project is under Creative Commons 

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Project status
One shot
