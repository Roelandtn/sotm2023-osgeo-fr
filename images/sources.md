images,source
logo-osgeo.svg,https://github.com/OSGeo/osgeo/tree/master/marketing/branding/logo
OpenStreetMap_logo.svg,https://wiki.openstreetmap.org/wiki/File:Public-images-osm_logo.svg 
https://2022.stateofthemap.org/programme/
https://2022.foss4g.org/

osm_data.png,https://download.osgeo.org/livedvd/data/osm/
OSM_Components.svg,https://wiki.openstreetmap.org/wiki/File:OSM_Components.svg
osgeo_community_projects.png,https://www.osgeo.org/projects/
osgeo_projects.png,https://www.osgeo.org/projects/
OSM_Tasking_Manager.png,https://www.osgeo.org/projects/hot-tasking-manager/
logo-sotm-2023.svg,https://sotm2023.openstreetmap.fr/
